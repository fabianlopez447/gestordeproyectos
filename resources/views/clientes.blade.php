@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Clientes<a href="#" class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#agregar">Añadir</a></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <center><h3 class="card-title">001</h3></center>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Nombre:</b> Carlos </li>
                                    <li class="list-group-item"><b>Apellido:</b> Perez</li>
                                    <li class="list-group-item"><b>Telefono:</b> +5855418562</li>
                                </ul>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <a href="#" style="border: 1px solid;" class="btn btn-deafault">Modificar</a>
                                        <a href="#" class="btn btn-success">Contactar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        Elemento-->
                       <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <center><h3 class="card-title">002</h3></center>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Nombre:</b> Manuel </li>
                                    <li class="list-group-item"><b>Apellido:</b> Salazar</li>
                                    <li class="list-group-item"><b>Telefono:</b> +585545552</li>
                                </ul>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <a href="#" style="border: 1px solid;" class="btn btn-deafault">Modificar</a>
                                        <a href="#" class="btn btn-success">Contactar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                     
<!--                        Elemento-->
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <center><h3 class="card-title">003</h3></center>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Nombre:</b> Pedro </li>
                                    <li class="list-group-item"><b>Apellido:</b> Fuentes</li>
                                    <li class="list-group-item"><b>Telefono:</b> +58554552</li>
                                </ul>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <a href="#" style="border: 1px solid;" class="btn btn-deafault">Modificar</a>
                                        <a href="#" class="btn btn-success">Contactar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('elements')
<div class="modal fade bs-example-modal-lg" id="agregar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     <div class="modal-header">
         Agregar Clientes
     </div>
      <div class="modal-body">
          <form action="">
              <div class="form-group">
                  <label for="nombre">Nombre</label>
                  <input type="text" name="nombre" id="nombre" class="form-control">
              </div>
              <div class="form-group">
                  <label for="tipo">Apellido</label>
                  <input type="text" name="apellido" id="apellido" class="form-control">
              </div>
              <div class="form-group">
                  <label for="tipo">Telefono</label>
                  <input type="text" name="apellido" id="apellido" class="form-control">
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>
@endsection