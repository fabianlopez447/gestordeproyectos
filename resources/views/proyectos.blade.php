@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Proyectos <a href="#" class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#agregar">Añadir</a></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <center><h3 class="card-title">Paginacion</h3></center>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Cliente: Ysmael Clemente</b></li>
                                    <li class="list-group-item text-success"><b>Finalizado</b></li>
                                </ul>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <a href="#" style="border: 1px solid;" class="btn btn-deafault">Modificar</a>
                                        <a href="#" style="border-bottom: 1px solid;border-top: 1px solid;" class="btn btn-default">Estado</a>
                                        <a href="#" style="border: 1px solid;" class="btn btn-default">Ver</a>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Inicio:</b><br>
                                            21/01/2018
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <b>Final:</b><br>
                                            31/01/2018
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        Elemento-->
                   
                       <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <center><h3 class="card-title">CWA</h3></center>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Cliente: Ysmael Clemente</b></li>
                                    <li class="list-group-item text-warning"><b>Pendiente</b></li>
                                </ul>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <a href="#" style="border: 1px solid;" class="btn btn-deafault">Modificar</a>
                                        <a href="#" style="border-bottom: 1px solid;border-top: 1px solid;" class="btn btn-default">Estado</a>
                                        <a href="#" style="border: 1px solid;" class="btn btn-default">Ver</a>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Inicio:</b><br>
                                            21/01/2018
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <b>Final:</b><br>
                                            31/01/2018
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        Elemento-->
                       <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <center><h3 class="card-title">MIC</h3></center>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Cliente: Ysmael Clemente</b></li>
                                    <li class="list-group-item text-danger"><b>Vencido</b></li>
                                </ul>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                       <a href="#" style="border: 1px solid;" class="btn btn-deafault">Modificar</a>
                                        <a href="#" style="border-bottom: 1px solid;border-top: 1px solid;" class="btn btn-default">Estado</a>
                                        <a href="#" style="border: 1px solid;" class="btn btn-default">Ver</a>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Inicio:</b><br>
                                            21/01/2018
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <b>Final:</b><br>
                                            31/01/2018
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('elements')
<div class="modal fade bs-example-modal-lg" id="agregar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     <div class="modal-header">
         Agregar proyecto
     </div>
      <div class="modal-body">
          <form action="">
              <div class="form-group">
                  <label for="nombre">Nombre</label>
                  <input type="text" name="nombre" id="nombre" class="form-control">
              </div>
              <div class="form-group">
                  <label for="tipo">Tipo de proyecto</label>
                  <select name="tipo" id="" class="form-control">
                      <option value="RSS">Redes sociales</option>
                      <option value="landing">Landing Page</option>
                      <option value="eshop">Tienda Virtual</option>
                  </select>
              </div>
              <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="inicio">Fecha de inicio</label>
                          <input type="date" class="form-control">
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="fin">Fecha de entrega</label>
                          <input type="date" class="form-control">
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <label for="cliente">Cliente</label>
                  <select name="cliente" class="form-control" id="">
                      <option value="1">Jose Quintal</option>
                      <option value="2">Ian Summerville</option>
                      <option value="3">Fabian Lopez</option>
                  </select>
              </div>
              <div class="row">
                  <div class="col-md-6">
                       <div class="form-group">
                          <label for="obs">Observaciones</label>
                          <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="info">Información</label>
                          <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                      </div>
                  </div>
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>
@endsection