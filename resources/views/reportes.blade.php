@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Reportes </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                           <center>
                            <div class="btn btn-success btn-lg">Generar</div>
                            </center>
                        </div>
<!--                        Elemento-->
                   
                       
                
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('elements')
<div class="modal fade bs-example-modal-lg" id="agregar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     <div class="modal-header">
         Agregar proyecto
     </div>
      <div class="modal-body">
          <form action="">
              <div class="form-group">
                  <label for="nombre">Nombre</label>
                  <input type="text" name="nombre" id="nombre" class="form-control">
              </div>
              <div class="form-group">
                  <label for="tipo">Tipo de proyecto</label>
                  <select name="tipo" id="" class="form-control">
                      <option value="RSS">Redes sociales</option>
                      <option value="landing">Landing Page</option>
                      <option value="eshop">Tienda Virtual</option>
                  </select>
              </div>
              <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="inicio">Fecha de inicio</label>
                          <input type="date" class="form-control">
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="fin">Fecha de entrega</label>
                          <input type="date" class="form-control">
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <label for="cliente">Cliente</label>
                  <select name="cliente" class="form-control" id="">
                      <option value="1">Jose Quintal</option>
                      <option value="2">Ian Summerville</option>
                      <option value="3">Fabian Lopez</option>
                  </select>
              </div>
              <div class="row">
                  <div class="col-md-6">
                       <div class="form-group">
                          <label for="obs">Observaciones</label>
                          <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="info">Información</label>
                          <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                      </div>
                  </div>
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>
@endsection