@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Cuentas<a href="#" class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#agregar">Añadir</a></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <center><h3 class="card-title">Carlos Perez</h3></center>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Titulo: </b>Instagram</li>
                                    <li class="list-group-item"><b>Usuario:</b>elrepuesto</li>
                                </ul>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                         <a href="#" style="border: 1px solid;" class="btn btn-deafault">Modificar</a>
                                        <a href="#" class="btn btn-success">Copiar Pw</a>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        Elemento-->
                   
                      <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <center><h3 class="card-title">Carlos Perez</h3></center>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Titulo: </b>Facebook</li>
                                    <li class="list-group-item"><b>Usuario:</b>zonab</li>
                                </ul>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                         <a href="#" style="border: 1px solid;" class="btn btn-deafault">Modificar</a>
                                        <a href="#" class="btn btn-success">Copiar Pw</a>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        Elemento-->
                     <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <center><h3 class="card-title">Manuel Salazar</h3></center>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Titulo: </b>Instagram</li>
                                    <li class="list-group-item"><b>Usuario:</b>elrepuesto</li>
                                </ul>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                         <a href="#" style="border: 1px solid;" class="btn btn-deafault">Modificar</a>
                                        <a href="#" class="btn btn-success">Copiar Pw</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('elements')
<div class="modal fade bs-example-modal-lg" id="agregar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     <div class="modal-header">
         Agregar Clientes
     </div>
      <div class="modal-body">
          <form action="">
             <div class="form-group">
                  <label for="cliente">Cliente</label>
                  <select name="cliente" class="form-control" id="">
                      <option value="1">Jose Quintal</option>
                      <option value="2">Ian Summerville</option>
                      <option value="3">Fabian Lopez</option>
                  </select>
              </div>
              <div class="form-group">
                  <label for="nombre">Nombre</label>
                  <input type="text" name="nombre" id="nombre" class="form-control">
              </div>
              <div class="form-group">
                  <label for="tipo">Usuario</label>
                  <input type="text" name="apellido" id="apellido" class="form-control">
              </div>
              <div class="form-group">
                  <label for="tipo">Contraseña</label>
                  <input type="text" name="apellido" id="apellido" class="form-control">
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>
@endsection