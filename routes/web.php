<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('proyectos', function() {
    return view('proyectos');
});
Route::get('clientes', function() {
    return view('clientes');
});
Route::get('cuentas', function() {
    return view('cuentas');
});
Route::get('reportes', function() {
    return view('reportes');
});